import { Request, Response } from 'express'
import todoModel from '../../Models/Todo'
import { Todo } from '../../Types/Todo'

export const getTodos = async (req: Request, res: Response): Promise<void> => {
    const todos: Todo[] = await todoModel.find()

    res.status(200).json(todos)
} 

export const getTodo = async (req: Request, res: Response): Promise<void> => {
    await todoModel.findById(req.params.id, (err, result) => {
        if(err){
            res.status(400).json({ error: err })
        }else{
            res.status(200).json({ result })
        }
    })
}

export const addTodo = async (req: Request, res: Response): Promise<void> => {
    const body: Pick<Todo, 'title' | 'status'> = req.body

    if(!body.title || !body.status){
        res.status(401).json({
            status: 401,
            message: `ValidationError: Todo validation failed: title:${body.title} status:${body.status}`
        })
        return
    }
    const newTodoModel = new todoModel({
        title: body.title,
        status: body.status
    })
    const newTodoSave = await newTodoModel.save()
    const updateAllTodoAfterSave = await todoModel.find()

    res.status(201).json({
        message: 'Todo succesfully added',
        newTodoSave,
        updateAllTodoAfterSave
    })
}

export const updateTodo = async (req: Request, res: Response): Promise<void> => {
    const {params: {id}, body} = req

    if(!body.title || !body.status || !id){
        res.status(401).json({
            status: 401,
            message: 'ValidationError: missing _id or body properties'
        })
        return
    }

    const updateFindTodo = await todoModel.findByIdAndUpdate({_id: id}, body)
    const updateAfterFind = await todoModel.find()

    if(!updateFindTodo){
        res.status(501).json({
            status: 501,
            message: 'edit todo failed. Not implemented'
        })
        return
    }

    res.status(200).json({
        message: 'Edited succesfully',
        updateFindTodo,
        updateAfterFind
    })
}

export const deleteTodo = async (req: Request, res: Response): Promise<void> => {
    const {params: {id}, body} = req

    if(!body.title || !body.status || !id){
        res.status(401).json({
            status: 401,
            message: 'ValidationError: missing _id or body properties'
        })
        return
    }

    const findTodoAndRemove = await todoModel.findByIdAndRemove()
    const updateTodoAfterRemove = await todoModel.find()

    if(!findTodoAndRemove){
        res.status(501).json({
            status: 501,
            message: 'Todo remove failed. Not implemented'
        })
        return
    }

    res.status(200).json({
        message: 'Succesfully remove todo',
        findTodoAndRemove,
        todos: updateTodoAfterRemove
    })
}