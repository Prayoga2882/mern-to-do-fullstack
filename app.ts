// import dependencies
import express from 'express'
import mongoose from 'mongoose'
import cors from 'cors'
import router from './Router'

//panggil fungsi express ke dalam app, port & database
const app = express()
const port = 8080
const {
    MONGODB_ATLAS_USERNAME,
    MONGODB_ATLAS_PASSWORD,
    MONGODB_ATLAS_DBNAME
} = process.env

// deklarasikan url database
const uri = `mongodb+srv://${MONGODB_ATLAS_USERNAME}:${MONGODB_ATLAS_PASSWORD}@cluster0.jljqo.mongodb.net/${MONGODB_ATLAS_DBNAME}?retryWrites=true&w=majority`;
const options = {useNewUrlParser: true, useUnifiedTopoloy: true}

//panggil fungsi cors
app.use(cors())
app.use(router)

//set mongoose dan connect mongoose
mongoose.set('useFindAndModify', true)
mongoose.connect(uri, options)
    .then(() => {
        app.listen(port, () => {
            console.info(`App is running at http://localhost:${port}`)
        })
    })
    .catch((error) => {
        throw error
    })


