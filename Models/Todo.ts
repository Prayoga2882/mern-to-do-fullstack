// wajib membuat model schema sebelum membuat controller endPoint
import { model, Schema } from 'mongoose'
import { Todo } from '../Types/Todo'

const TodoSchema: Schema = new Schema(
    {
        title: {
            type: String,
            required: true
        },
        status: {
            type: String,
            required: true
        }
    },
    { timestamps: true }
)

export default model<Todo>('Todo', TodoSchema)